import logging
import atexit
import sqlite3
import csv

logging.basicConfig(filename='log.log',format='%(asctime)s:%(levelname)s:%(message)s',level=logging.DEBUG)

#creation d'une dataBase si elle n'existe pas
def check_table():
    cursor.execute('''CREATE TABLE IF NOT EXISTS siv(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                        adresse_titulaire text,nom text,prenom text,immatriculation text,date_immatriculation text,
                        vin text ,marque text, denomination_commerciale text,
                        couleur text, carrosserie text,categorie text ,cylindree text,
                        energie text, places text , poids text , puissance text ,
                        type text , variante text , version text)''')

#deconnection de la dataBase
def close_connect(connection):
    if connection:
        connection.close()

        
#insertion des valeurs dans la base        
def insertion(contain,SaveId):
        logging.info('Insertion :'+str(contain)+" in " +str(SaveId))
        cursor.execute('INSERT INTO siv(adresse_titulaire,nom,prenom,immatriculation,date_immatriculation,vin ,marque, denomination_commerciale , couleur, carrosserie ,categorie ,cylindree , energie , places  , poids , puissance , type , variante , version ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',contain)
                        
#mise à jour d'une information déjà existante dans la base
def update(contain,SaveId):
        logging.debug('Update :'+str(contain))
        contain.append(SaveId)
        logging.info('A l\'indice :'+str(SaveId))
        cursor.execute("UPDATE siv SET adresse_titulaire=?,nom=?,prenom=?,immatriculation=?,date_immatriculation=?,vin=? ,marque=?, denomination_commerciale=? , couleur=?, carrosserie=? ,categorie=? ,cylindree=? , energie=? , places=?  , poids=? , puissance=? , type=? , variante=? , version=?  WHERE siv.id=?" ,contain)
                        
def remplissage():
    with open('auto.csv','r') as csvfile:
        reader=csv.reader(csvfile,delimiter=';') 
        nbligne=0

        for lines in reader:
            contain=lines
            SaveId=0
            insert=True
            
            if nbligne!= 0:    
                for row in cursor.execute('SELECT id, vin FROM siv '):
                        
                        if contain[5]==row[1] : #Si le vin est déjà existant, on met à jour les valeurs
                                logging.info('voici le saveID : '+str(SaveId)+' voici le row 0 : '+str(row[0]))
                                logging.warning('Test vin :'+str(contain[5]) + "==" + str(row[1]))
                                insert=False
                                SaveId=row[0]
                        
                if insert:
                        insertion(contain,SaveId)
                        
                else :
                        update(contain,SaveId)
                        
            nbligne=nbligne+1
  

#connection à la dataBase
connection = sqlite3.connect('automobileDB.sqlite3')

cursor = connection.cursor()

#deconnection de la dataBase lors de la fermeture du programme
atexit.register(close_connect,connection)

contain = []

check_table()

remplissage()

#sauvegarde des valeurs dans la dataBase
connection.commit()








